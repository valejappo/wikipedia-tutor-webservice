# Wikipedia TUTOR
**Wikipedia TUTOR** is a web service designed to streamline the management of mentorship programs on MediaWiki wikis using the GrowthExperiments extension. With a focus on flexibility and ease of use, it allows users to efficiently reassign mentors and mentees, ensuring optimal pairings and balanced workloads. Wikimedia OAuth login is required for secure access.

> Wikipedia TUTOR is accessible at https://tutor.toolforge.org. There, a demo version is available for you to try. 

## Features
This project includes the following key features to enhance the mentoring experience:

* **Reassign mentee**
  - Allows mentors and administrators to reassign a mentee to a different mentor. This feature ensures that mentees can be matched with the most suitable mentors based on their evolving needs and goals.

* **Reassign mentor**
  - Enables users to change their mentor and, mentors and administrators to reassign a mentor to a different mentee. This feature helps in maintaining a balanced mentor-mentee ratio and ensures that mentors are not overloaded with too many mentees.

## APIs
POST APIs can be used trough the  ```api.php``` endpoint.

The ```action``` parameter defines the call's intent. The possible values are ```reassign-mentees``` and ```reassign-mentor```.

If the action being called is ```reassign-mentees```, then the ```mentor``` parameter must be filled with the mentor's username.

If the action being called is ```reassign-mentor```, then the ```mentee``` parameter must be filled with the mentor's username.

Both these actions require login.

## Contributing
### Installation
1. Clone this git repository
```
git clone https://gitlab.wikimedia.org/valejappo/wikipedia-tutor-webservice
git clone https://gitlab.wikimedia.org/valcio/wikipedia-tutor-bot.git wikipedia-tutor-webservice/scripts
```
2. Move into the base directory
```
cd wikipedia-tutor-webservice
```
3. Install the required dependencies
```
composer install
```

### Requirements
* PHP 7.1^
* PHP Curl
* Composer
* Bash
* Git