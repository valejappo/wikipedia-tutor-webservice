<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/functions.php';

if(!isset( $_GET['oauth_verifier'])){
    // Redirect the user to the authorization URL.

    $client=new Tutor\OAuthClient();
    header( 'Location: ' . $client->getLoginURL() ); // This will then redirect to the POST method.

} else {
    // Manage OAuth callback and complete login.

    session_start();
    $auth=new Tutor\OAuth(new MediaWiki\OAuthClient\Token( $_SESSION['request_key'], $_SESSION['request_secret'] ), $_GET['oauth_verifier'] );

    $_SESSION['auth'] = serialize($auth);
    unset( $_SESSION['request_key'], $_SESSION['request_secret'] );

    // Example 1: get the authenticated user's identity.
    // $ident = $client->identify( $_SESSION['access_token'] );
    header( 'Location: ' . $_GET['returnto'] ?? '/' );

}