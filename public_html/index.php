<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/functions.php';


OOUI\Theme::setSingleton( new OOUI\WikimediaUITheme() );
OOUI\Element::setDefaultDir( 'ltr' );

$auth=$_SESSION['auth'] ? unserialize($_SESSION['auth']) : null;
$URLargs = explode("/", $_SERVER['REQUEST_URI']);

switch ($URLargs[1] ?? null):
  case 'login.php':
  case 'login':?>
        <?php include("login.php");?>
        <?php break;?>

  <?php case 'api.php':?>
  <?php case 'api': ?>
        <?php include("api.php"); ?>
        <?php break; ?>
    
  <?php default:?>
    <?php
        if(($URLargs[1] ?? "") == ""){
          $pageTitle='Welcome';
          $loadPage="landing.php";
        }
        else{
          $loadPage="app.php";
        }
      
    ?>
    <html>
        <head>
            <title><?php echo ($pageTitle ? $pageTitle.' - ' : '')?>Wikipedia TUTOR</title>
            <link rel="shortcur icon" href="https://upload.wikimedia.org/wikipedia/commons/thumb/9/96/Gnome-help.svg/135px-Gnome-help.svg.png">
            <meta name='viewport' content='initial-scale=1.0, maximum-scale=1.0, user-scalable=no' />
            <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
            <link href='/styles.css' rel='stylesheet'>
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
            
            <!-- jQuery -->
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
            <!-- OOjs  -->
            <script src="https://doc.wikimedia.org/oojs-ui/master/demos/node_modules/oojs/dist/oojs.js"></script>
            <!-- OOUI --> 
            <script src="https://doc.wikimedia.org/oojs-ui/master/demos/dist/oojs-ui.js"></script>
            <!-- OOUI theme: wikimedia-ui -->
            <link rel="stylesheet" href="https://doc.wikimedia.org/oojs-ui/master/demos/dist/oojs-ui-wikimediaui.css">
            <script src="https://doc.wikimedia.org/oojs-ui/master/demos/dist/oojs-ui-wikimediaui.js"></script>

        </head>
        <body> 
        <header id="header" class="base">
          <span id="left" class="base">
            <span class="txB base">Wikipedia TUTOR</span>
          </span>
          <span id="right" class="base">
            <?php
              if(isset($_SESSION['auth']))
                $auth=unserialize($_SESSION['auth']);
              if($auth ? $auth->isValid() : false){
                $userOptions=$auth->getUsername()." (<a href='/logout.php'>Log Out</a>)";
              }else
                $userOptions="<a href='/login.php?returnto=".basename($_SERVER['REQUEST_URI'])."/'>Log In</a>";
            
            
            echo $userOptions ?>
          </span>
        </header>
            <br/>
            <!-- Content -->
            <div id="content">
              <!-- Warning if JS is not enabled -->
              <div class="plain" id="warning" role="alert"><?php $warn=new OOUI\MessageWidget(['label' => 'WARNING! Service unavaiable: Please enable JavaScript on your browser. If you do have it enabled, try to refresh the window.']); $warn->setType('error'); echo $warn; ?></div>
              <script>
                $('#warning').hide();
              </script>

              <!-- Warning notice -->
              <?php if(file_get_contents($configFiles['warning']) != false && file_get_contents($configFiles['warning']) != ""): ?>
                <div class="plain" id="warning" role="alert"><?php $warn=new OOUI\MessageWidget(['label' => file_get_contents($configFiles['warning'])]); $warn->setType('warning'); echo $warn; ?></div>
              <?php endif ?>

              <?php include $loadPage; ?>

            </div>
            <hr width="90%" class="base">
            <footer id="footer" class="txG base">
            
            <p class="base">Developed by <a href="https://it.wikipedia.org/wiki/User:valcio"><i>valcio</i></a></p>
            <br/>
          </footer>
        </body>
    </html>
  <?php break; ?>
<?php endswitch ?>