<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/functions.php';

function responseManager($status, $data, $message){
    echo '{"status": "'.$status.'", "data": '.($data ?? 'null').', "message": '.($message ? '"'.$message.'"' : 'null').'}';
}
function errorManager($msg=null){
    responseManager("error", null, $msg);
}

if(isset($_POST["action"])){
        if($_POST["action"] == "reassign-mentee"){
            if(isset($_POST["mentor"]) && isset($_POST["wiki"])){
                $auth=$_SESSION['auth'] ? unserialize($_SESSION['auth']) : null;
                if($auth ? $auth->isValid() : false){
                    shell_exec(escapeshellcmd('sh ../reassign_multiwiki.sh '.$_POST["wiki"].' "'.$auth->getUsername().'" "'.$_POST["mentor"].'" >> log.txt'));
                    responseManager("success", null, $_POST["mentor"]."'s mentee's successfully reassigned.");
                } else {
                    errorManager('Login required');
                }
            }else {
                errorManager('Missing arguments');
            }
        } else if($_POST["action"] == "reassign-mentor"){
            if(isset($_POST["mentee"]) && isset($_POST["wiki"])){
                $auth=$_SESSION['auth'] ? unserialize($_SESSION['auth']) : null;
                if($auth ? $auth->isValid() : false){
                    $mentorsListUnformatted=json_decode(json_decode($auth->APICall('https://'.$_POST["wiki"].'.wikipedia.org/w/api.php?action=parse&format=json&page=MediaWiki:GrowthMentors.json&prop=wikitext&formatversion=2'))->parse->wikitext, true)["Mentors"];
                    $mentorsList=array();
                    foreach($mentorsListUnformatted as $mentor){
                        array_push($mentorsList, $mentor["username"]);
                    }
                    $csfr=json_decode($auth->APICall('https://'.$_POST["wiki"].'.wikipedia.org/w/api.php?action=query&format=json&meta=tokens&formatversion=2'))->query->tokens->csrftoken;
                    $data=[
                        "action" => "growthsetmentor",
                        "format" => "json",
                        "mentee" => $_POST["mentee"],
                        "mentor" => $mentorsList[rand(0, count($mentorsList)-1)],
                        "token" => $csfr,
                        "formatversion" => "2"
                    ];
                    $response=$auth->APICall('https://'.$_POST["wiki"].'.wikipedia.org/w/api.php', true, $data);
                    responseManager("success", json_encode($mentorsList), $_POST["mentee"]."'s mentor successfully reassigned.");
                } else {
                    errorManager('Login required');
                }
            }else {
                errorManager('Missing arguments');
            }
        }
        else {
            errorManager('Invalid action');
        }
} else if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    errorManager('Invalid request');
}